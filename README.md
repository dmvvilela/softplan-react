# Softplan Marvel

Este projeto é um requisito técnico para o processo seletivo da empresa Softplan.

#

## Observações

### Requisitos

A API MarvelQL estava fora do ar (falta de pagamento do servidor Vercel), portanto foi utilizada a [API oficial da Marvel](https://developer.marvel.com/docs#). Portanto, o diferencial de substituir o redux pelo local state management do Apollo Cliente não pôde ser feito.

Todas as funcionalidades foram corretamente implementadas. Ao clicar nos cards vai para a tela de detalhes e ao clicar no botão de edição vai para a tela de edição do personagem.

Ao clicar no input de busca é automaticamente iniciada a busca (navega para '/search').

### API

Normalmente eu colocaria as API Keys no arquivo .env e não enviaria para o repositório.
Porém, para facilitar a verificação do desafio técnico pelos gestores, estou colocando as keys publicamente no projeto (services/marvel.js).
Até porque necessitaria de um backend para verdadeiramente esconder a PRIVATE_KEY.

### CSS

Para deixar o projeto o mais simples possível (princípio do KISS) evitei utilizar frameworks de UI.
Portanto, não foi utilizado Styled Components, material-ui, tailwind CSS ou outros.
O CSS é feito diretamente pelo App.css/index.css e/ou inline nos elementos.
Em um projeto maior eu organizaria tais arquivos em um formato mais componentizado (poderia usar css-in-js por exemplo e cada componente ter seu próprio arquivo de estilos).

### Detalhes

Alguns detalhes foram deixados de lado por este código ser apenas uma demonstração técnica.
Mas algumas melhorias que poderiam ser feitas incluem: sanitize de html ao editar personagem, adicionar mais efeitos a botões, cache dos personagens marvel na tela principal, etc.

## Host

O site foi hospedado gratuitamente no netlify e pode ser acessado pelo seguinte link:

[https://frosty-euclid-d82847.netlify.app/](https://frosty-euclid-d82847.netlify.app/)

## CI/CD

O próprio Netlify já cria um pipeline onde ao fazer o commit de modificações no Gitlab,
ele inicia o build (yarn build) e realiza o deploy do website automaticamente.

O Gitlab também possui seu próprio pipeline de CI/CD e está configurado para um simples teste.

O arquivo .gitlab-ci.example.yml possui um outro script que evidenciaria o build -> tests -> deploy para o caso de uma aplicação maior, com muitos testes que devem ser realizados antes do deploy, etc.
