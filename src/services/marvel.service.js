import axios from 'axios';
import md5 from 'js-md5';

// Normalmente eu colocaria essas keys no arquivo .env e não enviaria para o repositório.
// Porém, para facilitar a verificação do desafio técnico pelos gestores,
// estou colocando as keys publicamente aqui. Até porque necessitaria de um backend
// para verdadeiramente esconder a PRIVATE_KEY.
const PUBLIC_KEY = 'b621317b27e453921686417baf1db597';
const PRIVATE_KEY = '3d26de31787efce78fa4f75040e90ad0d5390124';

const fetchMarvelAPI = (url, params) => {
  const timestamp = Number(new Date());
  const hash = md5.create();
  hash.update(timestamp + PRIVATE_KEY + PUBLIC_KEY);

  let apiurl = `${url}?ts=${timestamp}&apikey=${PUBLIC_KEY}&hash=${hash.hex()}`;
  if (params) apiurl += params;

  return axios.get(apiurl).then((resp) => resp.data.data.results);
};

export const fetchCharacters = (page = 1) => {
  const offset = 10 * (page - 1);

  return fetchMarvelAPI(
    'https://gateway.marvel.com/v1/public/characters',
    `&orderBy=name&limit=10&offset=${offset}`
  );
};

export const searchCharacter = (query) =>
  fetchMarvelAPI(
    'https://gateway.marvel.com/v1/public/characters',
    `&orderBy=name&limit=10&nameStartsWith=${query}`
  );
