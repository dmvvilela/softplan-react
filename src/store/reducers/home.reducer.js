import {
  SET_MARVEL_CHARACTERS,
  SET_MARVEL_CHARACTERS_SUCCESS,
  SET_MARVEL_CHARACTERS_ERROR,
  SET_MARVEL_DETAILS_CHARACTER,
  SET_MARVEL_EDIT_CHARACTER,
} from '../actions/actionTypes';

const initialState = {
  isLoading: false,
  errorMessage: '',
  marvelCharacters: [],
  characterDetails: {},
  characterEdit: {},
};

const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MARVEL_CHARACTERS:
      return {
        ...state,
        isLoading: true,
      };

    case SET_MARVEL_CHARACTERS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        errorMessage: '',
        marvelCharacters: action.characters,
      };

    case SET_MARVEL_CHARACTERS_ERROR:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error,
      };

    case SET_MARVEL_DETAILS_CHARACTER:
      return {
        ...state,
        characterDetails: action.character,
      };

    case SET_MARVEL_EDIT_CHARACTER:
      return {
        ...state,
        characterEdit: action.character,
      };

    default:
      return state;
  }
};

export default homeReducer;
