import {
  SET_MARVEL_CHARACTERS,
  SET_MARVEL_CHARACTERS_SUCCESS,
  SET_MARVEL_CHARACTERS_ERROR,
  SET_MARVEL_DETAILS_CHARACTER,
  SET_MARVEL_EDIT_CHARACTER,
} from './actionTypes';
import { fetchCharacters } from '../../services/marvel.service';

export const setMarvelCharactersSuccess = (characters) => ({
  type: SET_MARVEL_CHARACTERS_SUCCESS,
  characters,
});

export const setMarvelCharactersError = (status, error) => ({
  type: SET_MARVEL_CHARACTERS_ERROR,
  status,
  error,
});

export const setMarvelCharacters = (page) => async (dispatch) => {
  dispatch({
    type: SET_MARVEL_CHARACTERS,
    page,
  });

  try {
    const resp = await fetchCharacters(page);

    dispatch(setMarvelCharactersSuccess(resp));
  } catch (err) {
    console.log(err);
    dispatch(setMarvelCharactersError(err));
  }
};

export const setMarvelDetailsCharacter = (character) => ({
  type: SET_MARVEL_DETAILS_CHARACTER,
  character,
});

export const setMarvelEditCharacter = (character) => ({
  type: SET_MARVEL_EDIT_CHARACTER,
  character,
});
