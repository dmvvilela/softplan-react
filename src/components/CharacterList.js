import React from 'react';
import { CharacterCard } from '.';

const CharacterList = ({ characters }) => {
  if (!characters) return <div />;

  return characters.map((character) => (
    <CharacterCard key={character.name} character={character} />
  ));
};

export default CharacterList;
