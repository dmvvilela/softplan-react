import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import {
  setMarvelDetailsCharacter,
  setMarvelEditCharacter,
} from '../store/actions/home.actions';

const CharacterCard = ({ character }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  function handleClick() {
    dispatch(setMarvelDetailsCharacter(character));
    history.push('/details');
  }

  function handleEdit(e) {
    e.stopPropagation();
    dispatch(setMarvelEditCharacter(character));
    history.push('/edit');
  }

  return (
    <div className="character-card" onClick={handleClick}>
      <div className="edit-button" onClick={(e) => handleEdit(e)}>
        <img src="edit.svg" alt="Edit character" />
      </div>
      <div>
        <img
          className="character-image"
          src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
          alt="Thumbnail"
          style={{ objectFit: 'cover', transform: 'translateY(2px)' }}
        />
      </div>
      <div style={{ margin: '12px' }}>
        <h3 style={{ color: '#7a7a7a' }}>{character.name}</h3>
        <p className="character-description">{character.description}</p>
      </div>
    </div>
  );
};

CharacterCard.propTypes = {
  character: PropTypes.instanceOf(Object).isRequired,
};

export default CharacterCard;
