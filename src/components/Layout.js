import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { SearchCharacter } from '.';

const Layout = ({ children }) => {
  return (
    <div className="App">
      <Link to="/">
        <div style={{ transform: 'translateX(-30px)' }}>
          <img
            className="softplan-logo"
            src="softplan-logo.png"
            alt="Softplan logo"
          />
          <img
            className="marvel-logo"
            src="marvel-logo-red.png"
            alt="Marvel logo"
          />
        </div>
      </Link>
      <SearchCharacter />
      <div>{children}</div>
    </div>
  );
};

Layout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Layout;
