import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { setMarvelDetailsCharacter } from '../store/actions/home.actions';

const EditCharacter = ({ character }) => {
  const [name, setName] = useState(character.name);
  const [description, setDescription] = useState(character.description);

  const dispatch = useDispatch();
  const history = useHistory();

  function handleSave() {
    const newCharacter = { ...character, name, description };

    dispatch(setMarvelDetailsCharacter(newCharacter));
    history.push('/details');
  }

  return (
    <>
      <div>
        <img
          src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
          alt="Thumbnail"
          height="165"
          width="160"
          style={{ objectFit: 'cover', transform: 'translateY(2px)' }}
        />
      </div>
      <div style={{ margin: '16px' }}>
        <h2 className="edit-label" style={{ transform: 'translateX(-22px)' }}>
          Character Name
        </h2>
        <input value={name} onChange={(e) => setName(e.target.value)} />
        <h2 className="edit-label">Character Description</h2>
        <input
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </div>
      <div className="save-button" onClick={handleSave}>
        <strong>Save</strong>
      </div>
    </>
  );
};

EditCharacter.propTypes = {
  character: PropTypes.instanceOf(Object).isRequired,
};

export default EditCharacter;
