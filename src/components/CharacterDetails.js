import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { setMarvelEditCharacter } from '../store/actions/home.actions';

const CharacterDetails = ({ character }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  function handleEdit(e) {
    e.stopPropagation();
    dispatch(setMarvelEditCharacter(character));
    history.push('/edit');
  }

  if (!character.thumbnail) return <div />;

  return (
    <div className="character-details">
      <div className="edit-button" onClick={(e) => handleEdit(e)}>
        <img src="edit.svg" alt="Edit character" />
      </div>
      <div>
        <img
          src={`${character.thumbnail.path}.${character.thumbnail.extension}`}
          alt="Thumbnail"
          height="165"
          width="160"
          style={{ objectFit: 'cover', transform: 'translateY(2px)' }}
        />
      </div>
      <div style={{ margin: '16px' }}>
        <h1 style={{ color: '#7a7a7a' }}>{character.name}</h1>
        <p
          style={{
            maxWidth: 640,
            // textAlign: 'justify',
          }}
        >
          {character.description}
        </p>
      </div>
      <div>
        <h2>Comics</h2>
        {character.comics.items.map((comic) => (
          <p key={comic.name}>{comic.name}</p>
        ))}
      </div>
      <div>
        <h2>Series</h2>
        {character.series.items.map((serie) => (
          <p key={serie.name}>{serie.name}</p>
        ))}
      </div>
      <div>
        <h2>Stories</h2>
        {character.stories.items.map((storie) => (
          <p key={storie.name}>{storie.name}</p>
        ))}
      </div>
    </div>
  );
};

CharacterDetails.propTypes = {
  character: PropTypes.instanceOf(Object).isRequired,
};

export default CharacterDetails;
