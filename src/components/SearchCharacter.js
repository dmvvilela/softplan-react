import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { searchMarvelCharacter } from '../store/actions/search.actions';

const SearchCharacter = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  function onChangeInput(value) {
    dispatch(searchMarvelCharacter(value));
    history.push('/search');
  }

  return (
    <div className="character-list">
      <div className="search-character">
        <h3 style={{ opacity: 0.6, transform: 'translateX(16px)' }}>
          Search Marvel Character
        </h3>
        <input
          onChange={(e) => onChangeInput(e.target.value)}
          onFocus={() => history.push('/search')}
        />
      </div>
    </div>
  );
};

export default SearchCharacter;
