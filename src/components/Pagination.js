import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setMarvelCharacters } from '../store/actions/home.actions';

const Pagination = () => {
  const [page, setPage] = useState(1);

  const dispatch = useDispatch();

  function handlePageChange(direction) {
    if (page === 1 && direction === -1) return;

    const newPage = page + direction;
    setPage(newPage);
    dispatch(setMarvelCharacters(newPage));
  }

  return (
    <div className="pagination">
      <span onClick={() => handlePageChange(-1)}>{'<'}</span>
      <p>
        <strong>{page}</strong>
      </p>
      <span onClick={() => handlePageChange(1)}>{'>'}</span>
    </div>
  );
};

export default Pagination;
