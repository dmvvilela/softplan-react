import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import { Layout } from './components';
import { Home, Details, Search, Edit } from './pages';

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Layout>
          <div>
            <Switch>
              <Route path="/search">
                <Search />
              </Route>
              <Route path="/edit">
                <Edit />
              </Route>
              <Route path="/details">
                <Details />
              </Route>
              <Route path="/">
                <Home />
              </Route>
            </Switch>
          </div>
        </Layout>
      </Router>
    </Provider>
  );
};

export default App;
