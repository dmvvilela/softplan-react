import React from 'react';
import { useSelector } from 'react-redux';
import { CharacterList } from '../components';

const Search = () => {
  const isLoading = useSelector((state) => state.search.isLoading);
  const errorMessage = useSelector((state) => state.search.errorMessage);
  const marvelCharacters = useSelector(
    (state) => state.search.marvelCharacters
  );

  return (
    <div className="character-list">
      {isLoading ? (
        <p style={{ opacity: 0.8 }}>Loading...</p>
      ) : (
        <>
          <p>{errorMessage}</p>
          <CharacterList characters={marvelCharacters} />
        </>
      )}
    </div>
  );
};

export default Search;
