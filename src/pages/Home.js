import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setMarvelCharacters } from '../store/actions/home.actions';
import { CharacterList, Pagination } from '../components';

const Home = () => {
  const dispatch = useDispatch();

  const isLoading = useSelector((state) => state.home.isLoading);
  const errorMessage = useSelector((state) => state.home.errorMessage);
  const marvelCharacters = useSelector((state) => state.home.marvelCharacters);

  useEffect(() => {
    console.log('Fetching Marvel Characters...');
    dispatch(setMarvelCharacters());
  }, [dispatch]);

  return (
    <div className="character-list">
      {isLoading ? (
        <p style={{ opacity: 0.8 }}>Loading...</p>
      ) : (
        <>
          <p>{errorMessage}</p>
          <CharacterList characters={marvelCharacters} />
        </>
      )}
      <Pagination />
    </div>
  );
};

export default Home;
