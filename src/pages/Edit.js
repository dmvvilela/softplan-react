import React from 'react';
import { useSelector } from 'react-redux';
import { EditCharacter } from '../components';

const Edit = () => {
  const character = useSelector((state) => state.home.characterEdit);

  return (
    <div className="character-edit">
      <EditCharacter character={character} />
    </div>
  );
};

export default Edit;
