import React from 'react';
import { useSelector } from 'react-redux';
import { CharacterDetails } from '../components';

const Details = () => {
  const character = useSelector((state) => state.home.characterDetails);

  return (
    <div>
      <CharacterDetails character={character} />
    </div>
  );
};

export default Details;
