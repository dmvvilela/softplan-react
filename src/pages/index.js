import Home from './Home';
import Details from './Details';
import Search from './Search';
import Edit from './Edit';

export { Home, Details, Search, Edit };
